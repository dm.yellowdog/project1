#!/bin/bash
NAMESPACE="linkextractor"

if [ -n $(oc projects | grep "You are not a member of any projects") ]
then
    echo -e "\n--- Creating new project $NAMESPACE ---"
    oc new-project $NAMESPACE
    echo -e "\n--- Deploying ---"
    oc process -f openshift/template.yaml | oc create -f -
else
    oc project $NAMESPACE
    REDEPLOY="$RANDOM"
    echo -e "\n--- REDEPLOY: $REDEPLOY ---"
    echo -e "\n--- Deploying ---"
    oc process -f openshift/template.yaml REPLICA_COUNT="2" REDEPLOY="$REDEPLOY" | oc apply -f -
fi
